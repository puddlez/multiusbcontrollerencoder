/*
Multi-USB Controller Encoder v1.4
Multi-input USB keyboard encoder with passthrough to external encoders
15-DEC-2021

Written by Robert Lloyd
rl636018@gmail.com

Compatible with the following AVR microcontrollers:
at90usb646
at90usb1286

In Makefile, set MCU to device and F_CPU to clock speed
In Main.c, set CLKPR according to clock speed

To configure other devices, see device configuration section in UsbKeyboard.h

intended for compilation using avr-gcc, tested with v7.3.0
*/

#include <avr/io.h>
#include <util/delay.h>
#include "UsbKeyboard.c"
#include "ButtonMap.c"

void ReadRawInput(void);
void Debounce(void);
void SendKeys(void);
void SendButtons(void);

int main(void)
{
	CLKPR = 0x80, CLKPR = 0x00; // set clock prescale for 16 MHz clock
	
	// set I/O registers
	// -----------------
	// A0-2 button output		 (to external encoder)
	// A3-7 unused
	// B0-6 button output		 (to external encoder)
	// B7   unused
	// C0-3 ControllerType input (read from cable/adapter)
	// C4-5 Player input		 (1-4, read from player switch)
	// C6-7 aux button input	 (read from 3.5mm connector)
	// D0-6 controller I/O		 (default to input)
	// D7   aux button input	 (read from 3.5mm connector)
	// E0   OutputMode input	 (Nintendo enable, read from mode switch)
	// E1   OutputMode input	 (PC enable, read from mode switch)
	// E2-5 unused
	// E6-7 stick select output	 (to external encoder)
	// F0-7 button output		 (to external encoder)
	// enable pull-up resistors for inputs and default outputs high
	
	DDRE = 0b11000000;
	PORTE = 0b11111111;
	
	DDRA = 0b00000111;
	DDRB = 0b01111111;
	DDRC = 0b00000000;
	DDRD = 0b00000000;
	DDRF = 0b11111111;
	
	PORTA = 0b11111111;
	PORTB = 0b11111111;
	PORTC = 0b11111111;
	PORTD = 0b11111111;
	PORTF = 0b11111111;
	
	// read OutputMode from E0-1
	OutputMode = PINE & 0b00000011;
	
	// if OutputMode is PC,
	//   initialize USB communication
	//   wait until configuration is complete
	if (OutputMode == Output_PC)
	{
		InitializeUsb();
		while (UsbConfigured() == 0);
	}
	// if OutputMode is Switch, hold down B button for 150ms to set external encoder for Switch output
	else if (OutputMode == Output_Switch)
	{
		PORTF = 0b11101111;
		_delay_ms(150);
		PORTF = 0b11111111;
	}
	
    // main input loop
	while(1)
	{
		// read ControllerType from C0-3
		ControllerType = PINC & 0b00001111;
				
		 // if ControllerType has changed
		 //   set register D for controller i/o
		 //   update MapSelectButtons and ButtonMap
		if (ControllerType != ControllerType_Previous)
		{
			ControllerType_Previous = ControllerType;
						
			if (ControllerType == Controller_Arcade) DDRD = 0b01100000;		  // standard arcade stick
			else if (ControllerType == Controller_NES) DDRD = 0b01000110;	  // NES controller
			else if (ControllerType == Controller_SNES) DDRD = 0b01000110;	  // SNES controller
			else if (ControllerType == Controller_Genesis) DDRD = 0b01000000; // Genesis controller
			else if (ControllerType == Controller_PSX) DDRD = 0b01001110;	  // PSX controller
			
			ConfigureMapSelectButtons();
			ConfigureButtonMap(1);
		}
		
		// read Player from C4-5
		Player = ((PINC >> 4) & 0b00000011) + 1;
		
		// if Player has changed, update ButtonMap
		if (Player != Player_Previous)
		{
			Player_Previous = Player;
			
			ConfigureButtonMap(0);
		}
		
		// read RawInput according to ControllerType
		ReadRawInput();
		
		// debounce RawInput to determine ButtonState
		Debounce();
		
		// if MapSelectButtons are pressed, update ButtonMap
		if (((ButtonState[0] & MapSelectButtons[0]) == 0)
			&& ((ButtonState[1] & MapSelectButtons[1]) == 0)
			&& ((ButtonState[2] & MapSelectButtons[2]) == 0))
			ConfigureButtonMap(0);
		
		// if OutputMode is PC, update Keys currently pressed according to ButtonState and ButtonMap
		if (OutputMode == Output_PC)
			SendKeys();
		// if OutputMode is not PC, update ButtonOutput currently sent to external encoder according to ButtonState and ButtonMap
		else
			SendButtons();
	}
}

// debounces RawInput and determines ButtonState
void Debounce(void)
{
	// update DebounceTimer for each button according to CycleTime
	for (uint8_t button = 0; button < 20; button++)
	{
		if (DebounceTimer[button] >= CycleTime)
			DebounceTimer[button] -= CycleTime;
		else if (DebounceTimer[button] > 0)
			DebounceTimer[button] = 0;
	}
	
	// reset CycleTime to baseline
	CycleTime = 33;
	
	// if RawInput[0] is not equal to ButtonState[0],
	//   update ButtonState[0]
	//   record increase in CycleTime
	if (RawInput[0] != ButtonState[0])
	{
		// for each button in RawInput[0],
		//   if DebounceTimer has reached 0 and bit in RawInput[0] has changed,
		//     update bit in ButtonState[0]
		//     reset DebounceTimer
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
			if ((DebounceTimer[buttonBit] == 0)
				&& ((ButtonState[0] & (0b00000001 << buttonBit)) != (RawInput[0] & (0b00000001 << buttonBit))))
			{
				ButtonState[0] = (ButtonState[0] & ~(0b00000001 << buttonBit)) | RawInput[0];
				
				DebounceTimer[buttonBit] = DebouncePeriod;
			}
		
		CycleTime += 22;
	}
	
	// if RawInput[1] is not equal to ButtonState[1],
	//   update ButtonState[1]
	//   record increase in CycleTime
	if (RawInput[1] != ButtonState[1])
	{
		// for each button in RawInput[1],
		//   if DebounceTimer has reached 0 and bit in RawInput[1] has changed,
		//     update bit in ButtonState[1]
		//     reset DebounceTimer
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
			if ((DebounceTimer[buttonBit + 8] == 0)
				&& ((ButtonState[1] & (0b00000001 << buttonBit)) != (RawInput[1] & (0b00000001 << buttonBit))))
			{
				ButtonState[1] = (ButtonState[1] & ~(0b00000001 << buttonBit)) | RawInput[1];
				
				DebounceTimer[buttonBit + 8] = DebouncePeriod;
			}
		
		CycleTime += 22;
	}
	
	// if RawInput[2] is not equal to ButtonState[2],
	//   update ButtonState[2]
	//   record increase in CycleTime
	if (RawInput[2] != ButtonState[2])
	{
		// for each button in RawInput[2],
		//   if DebounceTimer has reached 0 and bit in RawInput[2] has changed,
		//     update bit in ButtonState[2]
		//     reset DebounceTimer
		for (uint8_t buttonBit = 0; buttonBit < 4; buttonBit++)
			if ((DebounceTimer[buttonBit + 16] == 0)
				&& ((ButtonState[2] & (0b00000001 << buttonBit)) != (RawInput[2] & (0b00000001 << buttonBit))))
			{
				ButtonState[2] = (ButtonState[2] & ~(0b00000001 << buttonBit)) | RawInput[2];
				
				DebounceTimer[buttonBit + 16] = DebouncePeriod;
			}
		
		CycleTime += 8;
	}
}

// updates Keys currently pressed according to ButtonState and ButtonMap
void SendKeys(void)
{	
	// if ButtonState has changed,
	//   update Keys
	//   increase CycleTime to 1ms
	if ((ButtonState[0] != ButtonState_Previous[0])
		|| (ButtonState[1] != ButtonState_Previous[1])
		|| (ButtonState[2] != ButtonState_Previous[2]))
	{
		ButtonState_Previous[0] = ButtonState[0];		
		ButtonState_Previous[1] = ButtonState[1];
		ButtonState_Previous[2] = ButtonState[2];
		
		// for each button in ButtonState[0],
		//   if bit is low, add key defined in ButtonMap to Keys
		//   if bit is high, add KEY_NONE to Keys
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
		{
			if (((ButtonState[0] >> buttonBit) & 0b00000001) == 0)
				Keys[buttonBit] = ButtonMap[buttonBit][0];
			else
				Keys[buttonBit] = KEY_NONE;
		}
		
		// for each button in ButtonState[1],
		//   if bit is low, add key defined in ButtonMap to Keys
		//   if bit is high, add KEY_NONE to Keys
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
		{
			if (((ButtonState[1] >> buttonBit) & 0b00000001) == 0)
				Keys[buttonBit + 8] = ButtonMap[buttonBit][1];
			else
				Keys[buttonBit + 8] = KEY_NONE;
		}
		
		// for each button in ButtonState[2],
		//   if bit is low, add key defined in ButtonMap to Keys
		//   if bit is high, add KEY_NONE to Keys
		for (uint8_t buttonBit = 0; buttonBit < 4; buttonBit++)
		{
			if (((ButtonState[2] >> buttonBit) & 0b00000001) == 0)
				Keys[buttonBit + 16] = ButtonMap[buttonBit][2];
			else
				Keys[buttonBit + 16] = KEY_NONE;
		}
		
		// update Keys currently pressed
		UsbKeyboard_HoldKeys();
		
		// increase CycleTime to 1ms
		CycleTime = 1000;
	}

	// record increase in CycleTime
	CycleTime += 2;
}

// updates ButtonOutput currently sent to external encoder according to ButtonState and ButtonMap
void SendButtons(void)
{
	// if ButtonState has changed,
	//   update ButtonOutput
	//   send OutputDirectionalType and ButtonOutput to external encoder
	//   record increase in CycleTime
	if ((ButtonState[0] != ButtonState_Previous[0])
	|| (ButtonState[1] != ButtonState_Previous[1])
	|| (ButtonState[2] != ButtonState_Previous[2]))
	{
		ButtonState_Previous[0] = ButtonState[0];
		ButtonState_Previous[1] = ButtonState[1];
		ButtonState_Previous[2] = ButtonState[2];
		
		// default ButtonOutput to none pressed
		ButtonOutput[0] = 0b11111111;
		ButtonOutput[1] = 0b11111111;
		ButtonOutput[2] = 0b11111111;
		
		if (((ButtonState[0] | ButtonMap[0][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[0][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[0][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111110;
		
		if (((ButtonState[0] | ButtonMap[1][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[1][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[1][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111101;
		
		if (((ButtonState[0] | ButtonMap[2][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[2][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[2][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111011;
		
		if (((ButtonState[0] | ButtonMap[3][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[3][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[3][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11110111;
		
		if (((ButtonState[0] | ButtonMap[4][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[4][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[4][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11101111;

		if (((ButtonState[0] | ButtonMap[5][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[5][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[5][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11011111;

		if (((ButtonState[0] | ButtonMap[6][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[6][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[6][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b10111111;

		if (((ButtonState[0] | ButtonMap[7][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[7][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[7][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b01111111;
		
		if (((ButtonState[0] | ButtonMap[8][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[8][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[8][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111110;
		
		if (((ButtonState[0] | ButtonMap[9][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[9][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[9][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111101;
		
		if (((ButtonState[0] | ButtonMap[10][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[10][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[10][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111011;
		
		if (((ButtonState[0] | ButtonMap[11][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[11][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[11][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11110111;
		
		if (((ButtonState[0] | ButtonMap[12][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[12][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[12][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11101111;

		if (((ButtonState[0] | ButtonMap[13][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[13][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[13][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11011111;
		
		if (((ButtonState[0] | ButtonMap[14][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[14][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[14][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b10111111;
		
		if (((ButtonState[0] | ButtonMap[15][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[15][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[15][2]) != 0b11111111))
			ButtonOutput[2] = ButtonOutput[2] & 0b11111110;
		
		if (((ButtonState[0] | ButtonMap[16][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[16][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[16][2]) != 0b11111111))
			ButtonOutput[2] = ButtonOutput[2] & 0b11111101;
		
		if (((ButtonState[0] | ButtonMap[17][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[17][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[17][2]) != 0b11111111))
			ButtonOutput[2] = ButtonOutput[2] & 0b11111011;
		
		// send OutputDirectionalType and ButtonOutput to external encoder
		PORTE = OutputDirectionalType;
		PORTF = ButtonOutput[0];
		PORTB = ButtonOutput[1];
		PORTA = ButtonOutput[2];
		
		// record increase in CycleTime
		CycleTime += 20;
	}
}

// reads RawInput according to ControllerType
void ReadRawInput(void)
{
	// read RawInput from aux buttons (read from 3.5mm connector)
	{
		RawInput[2] = (PINC >> 5) | 0b11111001;
		RawInput[2] = RawInput[2] & ((PIND >> 4) | 0b11110111);
	}
	
	// if ControllerType is arcade stick, read RawInput accordingly and record increase in CycleTime
	if (ControllerType == Controller_Arcade)
	{
		PORTD = 0b10011111;
		_delay_us(3);
		RawInput[0] = PIND & 0b00001111;
		
		PORTD = 0b10111111;
		_delay_us(3);
		RawInput[0] = RawInput[0] | (PIND << 4);
		
		PORTD = 0b11011111;
		_delay_us(3);
		RawInput[1] = PIND & 0b00001111;
		
		PORTD = 0b11111111;
		_delay_us(3);
		{
			uint8_t pind = PIND;
			
			RawInput[1] = RawInput[1] | (pind << 4);			
			RawInput[2] = RawInput[2] & ((pind >> 4) | 0b11111110);
		}
		
		// record increase in CycleTime
		CycleTime += 16;
	}
	// if ControllerType is NES controller, read RawInput accordingly and record increase in CycleTime
	else if (ControllerType == Controller_NES)
	{
		PORTD = 0b11111011;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = (PIND << 5) | 0b11011111;
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 4) | 0b11101111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = (PIND << 4) | 0b11101111;
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = RawInput[1] & ((PIND << 5) | 0b11011111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & (PIND | 0b11111110);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 1) | 0b11111101);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 2) | 0b11111011);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 3) | 0b11110111);
		
		// record increase in CycleTime
		CycleTime += 23;
	}
	// if ControllerType is SNES controller, read RawInput accordingly and record increase in CycleTime
	else if (ControllerType == Controller_SNES)
	{
		PORTD = 0b11111011;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = (PIND << 4) | 0b11101111;
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 6) | 0b10111111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = (PIND << 4) | 0b11101111;
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = RawInput[1] & ((PIND << 5) | 0b11011111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & (PIND | 0b11111110);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 1) | 0b11111101);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 2) | 0b11111011);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 3) | 0b11110111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 5) | 0b11011111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[0] = RawInput[0] & ((PIND << 7) | 0b01111111);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = RawInput[1] & (PIND | 0b11111110);
		
		PORTD = 0b11111101;
		_delay_us(2);
		PORTD = 0b11111001;
		RawInput[1] = RawInput[1] & ((PIND << 1) | 0b11111101);
		
		// record increase in CycleTime
		CycleTime += 34;
	}
	// if ControllerType is Genesis controller, read RawInput accordingly and record increase in CycleTime
	else if (ControllerType == Controller_Genesis)
	{
		// long delay required for controller to reset
		//   should be adjusted so that main input loop lasts approx 1.86ms when OutputMode is not PC
		_delay_ms(1.809);
		
		// cycle 0 - select low
		PORTD = 0b10111111;
		_delay_us(3);
		
		// cycle 1 - select high
		PORTD = 0b11111111;
		_delay_us(3);
		
		// cycle 2 - select low
		PORTD = 0b10111111;
		_delay_us(3);
		
		// cycle 3 - select high
		PORTD = 0b11111111;
		_delay_us(3);
		RawInput[0] = PIND;
		
		// cycle 4 - select low
		PORTD = 0b10111111;
		_delay_us(2);
		
		uint8_t input = PIND;
		uint8_t controllerType; // 3 = 3-button controller, 6 = 6-button controller
		
		if ((input & 0b00000011) == 0)
			controllerType = 6;
		else
			controllerType = 3;
		
		RawInput[0] = (RawInput[0] & 0b11101111) | (input & 0b00010000);
		RawInput[1] = input | 0b11011111;
		
		// cycle 5 - select high
		PORTD = 0b11111111;
		_delay_us(2);		
		input = PIND;		
		RawInput[0] = (RawInput[0] & 0b10011111) | ((input << 1) & 0b01100000);
		
		if (controllerType == 6)		
		{
			RawInput[0] = (RawInput[0] & 0b01111111) | ((input << 5) & 0b10000000);
			RawInput[1] = (RawInput[1] & 0b11111110) | ((input >> 1) & 0b00000001);
			RawInput[1] = (RawInput[1] & 0b11101101) | ((input << 1) & 0b00010010);
		}
		
		// record increase in CycleTime
		CycleTime += 1827;
	}
	// if ControllerType is PSX controller, read RawInput accordingly and record increase in CycleTime
	else if (ControllerType == Controller_PSX)
	{
		/*
		Note:
		Controller must be analog mode to output L3 and R3.
		Analog mode is only available when controller is polled at regular intervals.
		Must run all code in Debounce/SendKeys/SendButtons every cycle, even if RawInput/ButtonState doesn't change.
		*/
		
		// attention low
		PORTD = 0b11111001;
		_delay_us(8); // attention low delay
		
		
		
		// byte 1, bit 0
		PORTD = 0b11110101;
		_delay_us(2); // clock delay
		PORTD = 0b11111101;
		_delay_us(2); // clock delay
		
		// byte 1, bit 1
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 2
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 3
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 4
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 5
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 6
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 1, bit 7
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		PORTD = 0b11111001;
		_delay_us(8); // bit end delay
		
		
		
		// byte 2, bit 0
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 2, bit 1
		PORTD = 0b11110101;
		_delay_us(2); // clock delay
		PORTD = 0b11111101;
		_delay_us(2); // clock delay
		
		// byte 2, bit 2
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 2, bit 3
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 2, bit 4
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 2, bit 5
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 2, bit 6
		PORTD = 0b11110101;
		_delay_us(2); // clock delay
		PORTD = 0b11111101;
		_delay_us(2); // clock delay
		
		// byte 2, bit 7
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		PORTD = 0b11111001;
		_delay_us(8); // bit end delay
		
		
		
		// byte 3, bit 0
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 1
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 2
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 3
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 4
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 5
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 6
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 3, bit 7
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		PORTD = 0b11111001;
		_delay_us(8); // bit end delay
		
		
		
		// byte 4, bit 0
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = (PIND << 4) | 0b11101111; // Select
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 1
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		// RawInput[1] = RawInput[1] & ((PIND << 6) | 0b10111111); // L3
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 2
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		// RawInput[1] = RawInput[1] & ((PIND << 7) | 0b01111111); // R3
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 3
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = RawInput[1] & ((PIND << 5) | 0b11011111); // Start
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 4
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = PIND | 0b11111110; // Up
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 5
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 3) | 0b11110111); // Right
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 6
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 1) | 0b11111101); // Down
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 4, bit 7
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 2) | 0b11111011); // Left
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		PORTD = 0b11111001;
		_delay_us(8); // bit end delay
		
		
		
		// byte 5, bit 0
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = RawInput[1] & ((PIND << 2) | 0b11111011); // L2
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 1
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = RawInput[1] & ((PIND << 3) | 0b11110111); // R2
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 2
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = RawInput[1] & (PIND | 0b11111110); // L1
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 3
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[1] = RawInput[1] & ((PIND << 1) | 0b11111101); // R1
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 4
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 7) | 0b01111111); // Triangle
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 5
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 5) | 0b11011111); // Circle
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 6
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 4) | 0b11101111); // X
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		// byte 5, bit 7
		PORTD = 0b11110001;
		_delay_us(2); // clock delay
		RawInput[0] = RawInput[0] & ((PIND << 6) | 0b10111111); // Square
		PORTD = 0b11111001;
		_delay_us(2); // clock delay
		
		PORTD = 0b11111001;
		_delay_us(8); // bit end delay
		
		
		
		// attention high
		_delay_us(8); // attention high delay
		PORTD = 0b11111011;
		
		
		
		// record increase in CycleTime
		CycleTime += 233;
	}
	// if controller is not detected, reset RawInput (except aux buttons)
	else if (ControllerType == Controller_None)
	{
		RawInput[0] = 0b11111111;
		RawInput[1] = 0b11111111;
		RawInput[2] = RawInput[2] | 0b11110001;
	}
}