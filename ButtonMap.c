#include "UsbKeyboard.h"

uint8_t OutputMode = 0; // PC/console to output to

// possible valus for OutputMode
const uint8_t Output_PC = 0b00000001;      // PC (USB keyboard)
const uint8_t Output_PS_XBox = 0b00000011; // PS/XBox
const uint8_t Output_Switch = 0b00000010;  // Switch

uint8_t ControllerType = 0b00001111; // type of controller connected:
uint8_t ControllerType_Previous = 0b00001111;

// possible valus for ControllerType and ControllerType_Previous
const uint8_t Controller_Arcade = 0b00000000;  // arcade stick
const uint8_t Controller_NES = 0b00000010;     // NES controller
const uint8_t Controller_SNES = 0b00000011;    // SNES controller
const uint8_t Controller_Genesis = 0b00000100; // Genesis controller
const uint8_t Controller_PSX = 0b00000101;     // PSX controller
const uint8_t Controller_None = 0b00001111;    // no controller detected

uint8_t Player = 1; // player number (only applies when OutputMode is PC)
uint8_t Player_Previous = 1;

uint8_t OutputDirectionalType = 0b11111111; // type of directional output (doesn't apply when OutputMode is PC)

// possible valus for OutputDirectionalType:
const uint8_t Directional_D_Pad = 0b11111111;        // D-Pad
const uint8_t Directional_Left_Analog = 0b10111111;  // Left Analog
const uint8_t Directional_Right_Analog = 0b01111111; // Right Analog

uint8_t RawInput[3] = { 0b11111111, 0b11111111, 0b11111111 };     // raw button inputs before debouncing
uint8_t ButtonState[3] = { 0b11111111, 0b11111111, 0b11111111 };  // button states after debouncing
uint8_t ButtonState_Previous[3] = { 0b11111111, 0b11111111, 0b11111111 };
uint8_t ButtonOutput[3] = { 0b11111111, 0b11111111, 0b11111111 }; // button presses currently sent to external encoder

unsigned int DebounceTimer[20] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // time remaining until new state will be accepted for each button
unsigned int DebouncePeriod = 15000; // time to wait after a button state change before accepting a new button state
unsigned int CycleTime = 0; // time elapsed during each cycle (determined at runtime)
// all times in microseconds ("us")

uint8_t MapSelectButtons[3] = { 0b00000000, 0b00000000, 0b00000000 }; // button combination for map selection

// maps ButtonState to keys when OutputMode is PC)
// maps ButtonState to ButtonOutput using bit masks when OutputMode is not PC
uint8_t ButtonMap[18][3];

uint8_t Profile_Previous = 0; // most recent ButtonMap profile selected

/*
RawInput, ButtonState, and MapSelectButtons:
              index | 2| 2| 2| 2| 1| 1| 1| 1| 1| 1| 1| 1| 0| 0| 0| 0| 0| 0| 0| 0|
                bit | 3| 2| 1| 0| 7| 6| 5| 4| 3| 2| 1| 0| 7| 6| 5| 4| 3| 2| 1| 0|
                    -------------------------------------------------------------
       arcade stick |A3|A2|A1|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1|Rt|Lf|Dn|Up|
     NES controller |A3|A2|A1|  |  |  |St|Sl|  |  |  |  |  |  | A| B|Rt|Lf|Dn|Up|
    SNES controller |A3|A2|A1|  |  |  |St|Sl|  |  | R| L| X| Y| A| B|Rt|Lf|Dn|Up|
 Genesis controller |A3|A2|A1|  |  |  |St|Md|  |  | Z| Y| X| C| B| A|Rt|Lf|Dn|Up|
     PSX controller |A3|A2|A1|  |  |  |St|Sl|R2|L2|R1|L1|Tr|Sq|Cr| X|Rt|Lf|Dn|Up|
                    -------------------------------------------------------------
ButtonMap:          |19|18|17|16|15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1| 0|
                    -------------------------------------------------------------
Output:         PS3          |R3|L3|PS|St|Sl|R2|L2|R1|L1|Tr|Sq|Cr| X|Rt|Lf|Dn|Up|
            PS4/PS5       |TP|R3|L3|PS|Op|Sh|R2|L2|R1|L1|Tr|Sq|Cr| X|Rt|Lf|Dn|Up|
XBox 360/One/Series          |RS|LS|Xb|St|Bk|RT|LT|RB|LB| Y| X| B| A|Rt|Lf|Dn|Up|
             Switch       |Cp|RS|LS|Hm| +| -|ZR|ZL| R| L| X| Y| A| B|Rt|Lf|Dn|Up|

input buttons A1, A2, A3 are aux buttons (read from 3.5mm connector)
buttons mapped to output button 255 are disabled
*/

/*
Notes:

PC Mode:

	Player switch selects keys for player 1-4

	Aux Buttons (read from 3.5mm connector)
		Player 1 or 2:
			1: Coin 1
			2: Coin 2
			3: disabled
		Player 3 or 4:
			1: Coin 3
			2: Coin 4
			3: disabled
		
PS/Xbox and Switch Modes:

	Player switch selects directional output:
		1: D-Pad
		2: Left Analog
		3: Right Analog
		4: D-Pad
	
	Aux Buttons disabled
*/

// updates MapSelectButtons
void ConfigureMapSelectButtons(void)
{
	// if ControllerType is arcade stick, button 11 selects button map
	if (ControllerType == Controller_Arcade)
	{
		MapSelectButtons[0] = 0b00000000;
		MapSelectButtons[1] = 0b01000000;
		MapSelectButtons[2] = 0b00000000;
	}
	// if ControllerType is not arcade stick, buttons Select/Mode and Start together select button map
	else 
	{
		MapSelectButtons[0] = 0b00000000;
		MapSelectButtons[1] = 0b00110000;
		MapSelectButtons[2] = 0b00000000;
	}
}

// updates ButtonMap
// profile = 1-8 to select specified profile, 0 to select profile according to directionalInput
void ConfigureButtonMap(uint8_t profile)
{
	// if profile = 0, select profile according to directionalInput
	if (profile == 0)
	{
		// read directionalInput
		uint8_t directionalInput = ButtonState[0] | 0b11110000;
		
		// select profile according to directionalInput
		if (directionalInput == 0b11111110)		 // up
			profile = 1;
		else if (directionalInput == 0b11111101) // down
			profile = 2;
		else if (directionalInput == 0b11111011) // left
			profile = 3;
		else if (directionalInput == 0b11110111) // right
			profile = 4;
		else if (directionalInput == 0b11111010) // up+left
			profile = 5;
		else if (directionalInput == 0b11110101) // down+right
			profile = 6;
		else if (directionalInput == 0b11111001) // down+left
			profile = 7;
		else if (directionalInput == 0b11110110) // up+right
			profile = 8;
		else									 // default to most recent profile if no direction is pressed
			profile = Profile_Previous;
	}
	
	Profile_Previous = profile;
	
	// define buttonMap
	uint8_t buttonMap[20];
	{
		if (OutputMode == Output_PC)
		{
			// ButtonMap: PC, arcade stick
			if (ControllerType == Controller_Arcade)
			{
				if (profile == 1) // up, default
				{
					// MAME keyboard inputs
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_I;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_5;
						buttonMap[16] = KEY_5;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_COMMA;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_6;
						buttonMap[16] = KEY_6;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_7;
						buttonMap[16] = KEY_7;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_6;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_8;
						buttonMap[16] = KEY_8;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 2) // down			
				{
					// MAME keyboard inputs, 6-button fighter
					// (players 3 and 4 disabled)
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_I;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_P;
						buttonMap[9] = KEY_J;
						buttonMap[10] = KEY_K;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_5;
						buttonMap[16] = KEY_5;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_COMMA;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_SLASH;
						buttonMap[9] = KEY_C;
						buttonMap[10] = KEY_V;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_6;
						buttonMap[16] = KEY_6;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if ((Player == 3) || (Player == 4))
					{
						buttonMap[0] = KEY_NONE;
						buttonMap[1] = KEY_NONE;
						buttonMap[2] = KEY_NONE;
						buttonMap[3] = KEY_NONE;
						buttonMap[4] = KEY_NONE;
						buttonMap[5] = KEY_NONE;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_NONE;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 3) // left
				{
					// MAME keyboard inputs
					// buttons 1 and 2 swapped
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_U;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_5;
						buttonMap[16] = KEY_5;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_M;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_6;
						buttonMap[16] = KEY_6;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_7;
						buttonMap[16] = KEY_7;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_5;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_8;
						buttonMap[16] = KEY_8;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 4) // right
				{
					// MAME keyboard inputs
					// buttons 2 and 3 swapped
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_O;
						buttonMap[6] = KEY_I;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_5;
						buttonMap[16] = KEY_5;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_PERIOD;
						buttonMap[6] = KEY_COMMA;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_6;
						buttonMap[16] = KEY_6;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_QUOTE;
						buttonMap[6] = KEY_X;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_7;
						buttonMap[16] = KEY_7;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_7;
						buttonMap[6] = KEY_NUMPAD_6;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_8;
						buttonMap[16] = KEY_8;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 5) // up+left
				{
					// Player 1
					if (Player == 1)
					{
						// Touhou Project
						// joystick  = Arrow Keys
						// button 1  = Z
						// button 2  = Left Shift
						// button 3  = X
						// button 4  = C
						// button 10 = Escape
						buttonMap[0] = KEY_UP;
						buttonMap[1] = KEY_DOWN;
						buttonMap[2] = KEY_LEFT;
						buttonMap[3] = KEY_RIGHT;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_LEFTSHIFT;
						buttonMap[6] = KEY_X;
						buttonMap[7] = KEY_C;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_ESC;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}
					// Player 2
					else if (Player == 2)
					{
						// Hydorah
						// joystick  = Arrow Keys
						// button 1  = Z
						// button 2  = X
						// button 10 = Enter
						// button 11 = Escape
						buttonMap[0] = KEY_UP;
						buttonMap[1] = KEY_DOWN;
						buttonMap[2] = KEY_LEFT;
						buttonMap[3] = KEY_RIGHT;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_ENTER;
						buttonMap[14] = KEY_ESC;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}
					// Player 3
					else if (Player == 3)
					{
						// Tumiki Fighters
						// joystick  = Arrow Keys
						// button 1  = Z
						// button 2  = X
						// button 10 = P
						// button 11 = Escape
						buttonMap[0] = KEY_UP;
						buttonMap[1] = KEY_DOWN;
						buttonMap[2] = KEY_LEFT;
						buttonMap[3] = KEY_RIGHT;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_P;
						buttonMap[14] = KEY_ESC;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}
					// Player 4
					else if (Player == 4)
					{
						// Touhou Project (alternate)
						// joystick  = Arrow Keys
						// button 1  = Z
						// button 2  = X
						// button 3  = C
						// button 4  = V
						// button 10 = Escape
						buttonMap[0] = KEY_UP;
						buttonMap[1] = KEY_DOWN;
						buttonMap[2] = KEY_LEFT;
						buttonMap[3] = KEY_RIGHT;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[6] = KEY_C;
						buttonMap[7] = KEY_V;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_ESC;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}				
				}
				else if (profile == 6) // down+right
				{
					// Player 1
					if (Player == 1)
					{
						// Bloodstained: Curse of the Moon
						// button 4  = J
						// button 5  = K
						// button 6  = Enter
						// button 7  = Escape
						// button 8  = L
						// button 10 = P
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_I;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_J;
						buttonMap[8] = KEY_K;
						buttonMap[9] = KEY_ENTER;
						buttonMap[10] = KEY_ESC;
						buttonMap[11] = KEY_L;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_P;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_NONE;
						buttonMap[16] = KEY_NONE;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					// Player 2
					{
						// TMNT: Shredders's Revenge
						// joystick  = Arrow Keys
						// button 1  = X
						// button 2  = Z
						// button 3  = S
						// button 5  = Left Ctrl
						// button 6  = Left Shift
						// button 7  = A
						// button 8  = H
						// button 9  = Escape
						// button 10 = Enter
						// button 12 = Q
						// button 13 = E
						buttonMap[0] = KEY_UP;
						buttonMap[1] = KEY_DOWN;
						buttonMap[2] = KEY_LEFT;
						buttonMap[3] = KEY_RIGHT;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[6] = KEY_S;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_LEFTCTRL;
						buttonMap[9] = KEY_LEFTSHIFT;
						buttonMap[10] = KEY_A;
						buttonMap[11] = KEY_H;
						buttonMap[12] = KEY_ESC;
						buttonMap[13] = KEY_ENTER;
						buttonMap[14] = KEY_NONE;
						buttonMap[15] = KEY_Q;
						buttonMap[16] = KEY_E;
						buttonMap[17] = KEY_NONE;
						buttonMap[18] = KEY_NONE;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3) { }
					else if (Player == 4) { }
				}
				else if (profile == 7) { }
				else if (profile == 8) // up+right
				{
					// Input Test
					buttonMap[0] = KEY_0;
					buttonMap[1] = KEY_1;
					buttonMap[2] = KEY_2;
					buttonMap[3] = KEY_3;
					buttonMap[4] = KEY_4;
					buttonMap[5] = KEY_5;
					buttonMap[6] = KEY_6;
					buttonMap[7] = KEY_7;
					buttonMap[8] = KEY_8;
					buttonMap[9] = KEY_9;
					buttonMap[10] = KEY_A;
					buttonMap[11] = KEY_B;
					buttonMap[12] = KEY_C;
					buttonMap[13] = KEY_D;
					buttonMap[14] = KEY_E;
					buttonMap[15] = KEY_F;
					buttonMap[16] = KEY_G;
					buttonMap[17] = KEY_H;
					buttonMap[18] = KEY_I;
					buttonMap[19] = KEY_J;
				}
			}
			// ButtonMap: PC, NES controller
			else if (ControllerType == Controller_NES)
			{
				if (profile == 1) // up, default
				{
					// MAME keyboard inputs
					// A = button 1
					// B = button 2
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_U;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_M;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_5;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 2) // down
				{
					// MAME keyboard inputs
					// B = button 1
					// A = button 2
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_I;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_COMMA;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_6;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 3) { }
				else if (profile == 4) { }
				else if (profile == 5) { }
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}
			// ButtonMap: PC, SNES controller
			else if (ControllerType == Controller_SNES)
			{
				if (profile == 1) // up, default
				{
					// MAME keyboard inputs
					// A = button 1
					// B = button 2
					// Y = button 3
					// X = button 4
					// L = button 5
					// R = button 6
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_U;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_M;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_5;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 2) // down
				{
					// MAME keyboard inputs, 6-button fighter
					// Y = button 1
					// X = button 2
					// L = button 3
					// B = button 4
					// A = button 5
					// R = button 6
					// (players 3 and 4 disabled)
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_P;
						buttonMap[5] = KEY_J;
						buttonMap[6] = KEY_U;
						buttonMap[7] = KEY_I;
						buttonMap[8] = KEY_O;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_SLASH;
						buttonMap[5] = KEY_C;
						buttonMap[6] = KEY_M;
						buttonMap[7] = KEY_COMMA;
						buttonMap[8] = KEY_PERIOD;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if ((Player == 3) || (Player == 4))
					{
						buttonMap[0] = KEY_NONE;
						buttonMap[1] = KEY_NONE;
						buttonMap[2] = KEY_NONE;
						buttonMap[3] = KEY_NONE;
						buttonMap[4] = KEY_NONE;
						buttonMap[5] = KEY_NONE;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_NONE;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 3) // left
				{
					// MAME keyboard inputs
					// Y = button 1
					// B = button 2
					// A = button 3
					// X = button 4
					// L = button 5
					// R = button 6
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_O;
						buttonMap[6] = KEY_U;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_PERIOD;
						buttonMap[6] = KEY_M;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_QUOTE;
						buttonMap[6] = KEY_Z;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_7;
						buttonMap[6] = KEY_NUMPAD_5;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 4) // right
				{
					// MAME keyboard inputs
					// B = button 1
					// Y = button 2
					// A = button 3
					// X = button 4
					// L = button 5
					// R = button 6
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_O;
						buttonMap[6] = KEY_I;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_PERIOD;
						buttonMap[6] = KEY_COMMA;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_QUOTE;
						buttonMap[6] = KEY_X;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_7;
						buttonMap[6] = KEY_NUMPAD_6;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 5) { }
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}
			// ButtonMap: PC, Genesis Controller
			else if (ControllerType == Controller_Genesis)
			{
				if (profile == 1) // up, default
				{
					// MAME keyboard inputs
					// A = button 1
					// B = button 2
					// C = button 3
					// X = button 4
					// Y = button 5
					// Z = button 6
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_I;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_COMMA;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_X;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_6;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 2) // down
				{
					// MAME keyboard inputs, 6-button fighter
					// X = button 1
					// Y = button 2
					// Z = button 3
					// A = button 4
					// B = button 5
					// C = button 6
					// (players 3 and 4 disabled)
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_P;
						buttonMap[5] = KEY_J;
						buttonMap[6] = KEY_K;
						buttonMap[7] = KEY_U;
						buttonMap[8] = KEY_I;
						buttonMap[9] = KEY_O;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_SLASH;
						buttonMap[5] = KEY_C;
						buttonMap[6] = KEY_V;
						buttonMap[7] = KEY_M;
						buttonMap[8] = KEY_COMMA;
						buttonMap[9] = KEY_PERIOD;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}				
					else if ((Player == 3) || (Player == 4))
					{
						buttonMap[0] = KEY_NONE;
						buttonMap[1] = KEY_NONE;
						buttonMap[2] = KEY_NONE;
						buttonMap[3] = KEY_NONE;
						buttonMap[4] = KEY_NONE;
						buttonMap[5] = KEY_NONE;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_NONE;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 3) // left
				{
					// MAME keyboard inputs
					// B = button 1
					// A = button 2
					// C = button 3
					// X = button 4
					// Y = button 5
					// Z = button 6
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_U;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_M;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_5;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 4) // right			
				{
					// d-pad = arrow keys
					// keys match buttons
					// A, B, C, X, Y, Z = A, B, C, X, Y, Z
					// Start = S
					// Mode = M
					// (player switch is ignored)
					buttonMap[0] = KEY_UP;
					buttonMap[1] = KEY_DOWN;
					buttonMap[2] = KEY_LEFT;
					buttonMap[3] = KEY_RIGHT;
					buttonMap[4] = KEY_A;
					buttonMap[5] = KEY_B;
					buttonMap[6] = KEY_C;
					buttonMap[7] = KEY_X;
					buttonMap[8] = KEY_Y;
					buttonMap[9] = KEY_Z;
					buttonMap[12] = KEY_M;
					buttonMap[13] = KEY_S;
					buttonMap[17] = KEY_NONE;
					buttonMap[18] = KEY_NONE;
					buttonMap[19] = KEY_NONE;
				}
				else if (profile == 5) { }
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}
			// ButtonMap: PC, PSX controller
			else if (ControllerType == Controller_PSX)
			{
				if (profile == 1) // up, default
				{
					// MAME keyboard inputs
					// Cr = button 1
					// X  = button 2
					// Sq = button 3
					// Tr = button 4
					// L1 = button 5
					// R1 = button 6
					// L2 = button 7
					// R2 = button 8
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_U;
						buttonMap[6] = KEY_O;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_M;
						buttonMap[6] = KEY_PERIOD;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_Z;
						buttonMap[6] = KEY_QUOTE;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_5;
						buttonMap[6] = KEY_NUMPAD_7;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 2) // down
				{
					// MAME keyboard inputs, 6-button fighter
					// Sq	 = button 1
					// Tr	 = button 2
					// L1/L2 = button 3
					// X	 = button 4
					// Cr	 = button 5
					// R1/R2 = button 6
					// (players 3 and 4 disabled)
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_P;
						buttonMap[5] = KEY_J;
						buttonMap[6] = KEY_U;
						buttonMap[7] = KEY_I;
						buttonMap[8] = KEY_O;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_O;
						buttonMap[11] = KEY_K;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_SLASH;
						buttonMap[5] = KEY_C;
						buttonMap[6] = KEY_M;
						buttonMap[7] = KEY_COMMA;
						buttonMap[8] = KEY_PERIOD;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_PERIOD;
						buttonMap[11] = KEY_V;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if ((Player == 3) || (Player == 4))
					{
						buttonMap[0] = KEY_NONE;
						buttonMap[1] = KEY_NONE;
						buttonMap[2] = KEY_NONE;
						buttonMap[3] = KEY_NONE;
						buttonMap[4] = KEY_NONE;
						buttonMap[5] = KEY_NONE;
						buttonMap[6] = KEY_NONE;
						buttonMap[7] = KEY_NONE;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_NONE;
						buttonMap[13] = KEY_NONE;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 3) // left
				{
					// MAME keyboard inputs
					// Y  = button 1
					// B  = button 2
					// A  = button 3
					// X  = button 4
					// L1 = button 5
					// R1 = button 6
					// L2 = button 7
					// R2 = button 8
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_I;
						buttonMap[5] = KEY_O;
						buttonMap[6] = KEY_U;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_COMMA;
						buttonMap[5] = KEY_PERIOD;
						buttonMap[6] = KEY_M;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_X;
						buttonMap[5] = KEY_QUOTE;
						buttonMap[6] = KEY_Z;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_6;
						buttonMap[5] = KEY_NUMPAD_7;
						buttonMap[6] = KEY_NUMPAD_5;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 4) // right
				{
					// MAME keyboard inputs
					// X  = button 1
					// Sq = button 2
					// Cr = button 3
					// Tr = button 4
					// L1 = button 5
					// R1 = button 6
					// L2 = button 7
					// R2 = button 8
					if (Player == 1)
					{
						buttonMap[0] = KEY_W;
						buttonMap[1] = KEY_S;
						buttonMap[2] = KEY_A;
						buttonMap[3] = KEY_D;
						buttonMap[4] = KEY_U;
						buttonMap[5] = KEY_O;
						buttonMap[6] = KEY_I;
						buttonMap[7] = KEY_P;
						buttonMap[8] = KEY_J;
						buttonMap[9] = KEY_K;
						buttonMap[10] = KEY_L;
						buttonMap[11] = KEY_SEMICOLON;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_1;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 2)
					{
						buttonMap[0] = KEY_T;
						buttonMap[1] = KEY_G;
						buttonMap[2] = KEY_F;
						buttonMap[3] = KEY_H;
						buttonMap[4] = KEY_M;
						buttonMap[5] = KEY_PERIOD;
						buttonMap[6] = KEY_COMMA;
						buttonMap[7] = KEY_SLASH;
						buttonMap[8] = KEY_C;
						buttonMap[9] = KEY_V;
						buttonMap[10] = KEY_B;
						buttonMap[11] = KEY_N;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_2;
						buttonMap[17] = KEY_5;
						buttonMap[18] = KEY_6;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 3)
					{
						buttonMap[0] = KEY_Q;
						buttonMap[1] = KEY_E;
						buttonMap[2] = KEY_R;
						buttonMap[3] = KEY_Y;
						buttonMap[4] = KEY_Z;
						buttonMap[5] = KEY_QUOTE;
						buttonMap[6] = KEY_X;
						buttonMap[7] = KEY_BACKSLASH;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_3;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
					else if (Player == 4)
					{
						buttonMap[0] = KEY_NUMPAD_1;
						buttonMap[1] = KEY_NUMPAD_2;
						buttonMap[2] = KEY_NUMPAD_3;
						buttonMap[3] = KEY_NUMPAD_4;
						buttonMap[4] = KEY_NUMPAD_5;
						buttonMap[5] = KEY_NUMPAD_7;
						buttonMap[6] = KEY_NUMPAD_6;
						buttonMap[7] = KEY_NUMPAD_8;
						buttonMap[8] = KEY_NONE;
						buttonMap[9] = KEY_NONE;
						buttonMap[10] = KEY_NONE;
						buttonMap[11] = KEY_NONE;
						buttonMap[12] = KEY_SPACE;
						buttonMap[13] = KEY_4;
						buttonMap[17] = KEY_7;
						buttonMap[18] = KEY_8;
						buttonMap[19] = KEY_NONE;
					}
				}
				else if (profile == 5) { }
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}		
		}
		else if (OutputMode == Output_PS_XBox)
		{
			// ButtonMap: PS/XBox, arcade stick
			if (ControllerType == Controller_Arcade)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// button 9 = Touch Pad
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 17;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) // down
				{
					// button 1 = Circle
					// button 2 = X
					// button 9 = Touch Pad
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 17;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 3) // left
				{
					// button 1 = X
					// button 2 = Square
					// button 3 = Circle					
					// button 9 = Touch Pad
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[6] = 5;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 17;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// button 1 = Square
					// button 2 = X
					// button 3 = Circle
					// button 9 = Touch Pad
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 17;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// button 9 = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) // down+right
				{
					// button 1 = Circle
					// button 2 = X
					// button 9 = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 7) // down+left
				{
					// button 1 = X
					// button 2 = Square
					// button 3 = Circle
					// button 9 = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[6] = 5;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// button 1 = Square
					// button 2 = X
					// button 3 = Circle
					// button 9 = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}
			// ButtonMap: PS/XBox, NES controller
			else if (ControllerType == Controller_NES)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// A = X
					// B = Cr
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) // down
				{
					// B = X
					// A = Cr
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 3) // left
				{
					// A = X
					// B = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// B = X
					// A = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// A = X
					// B = Cr
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) // down+right
				{
					// B = X
					// A = Cr
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 7) // down+left
				{
					// A = X
					// B = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// B = X
					// A = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}
			// ButtonMap: PS/XBox, SNES controller
			else if (ControllerType == Controller_SNES)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) // left
				{
					// rotate face buttons counter-clockwise
					// A = X, X = Cr, Y = Tr, B = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// rotate face buttons clockwise
					// Y = X, B = Cr, A = Tr, X = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) // down+left
				{
					// rotate face buttons counter-clockwise
					// A = X, X = Cr, Y = Tr, B = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// rotate face buttons clockwise
					// Y = X, B = Cr, A = Tr, X = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}		
			// ButtonMap: PS/XBox, Genesis Controller
			else if (ControllerType == Controller_Genesis)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// A = Sq
					// B = X
					// C = Cr
					// X = L1
					// Y = Tr
					// Z = R1
					// Mode = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 8;
					buttonMap[8] = 7;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) { }
				else if (profile == 4) { }
				else if (profile == 5) // up+left
				{
					// A = Sq
					// B = X
					// C = Cr
					// X = L1
					// Y = Tr
					// Z = R1
					// Mode = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 8;
					buttonMap[8] = 7;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}
			// ButtonMap: PS/XBox, PSX controller
			else if (ControllerType == Controller_PSX)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) // left
				{
					// rotate face buttons counter-clockwise
					// Cr = X, Tr = Cr, Sq = Tr, X = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// rotate face buttons clockwise
					// Sq = X, X = Cr, Cr = Tr, Tr = Sq
					// Select = PS button
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) // down+left
				{
					// rotate face buttons counter-clockwise
					// Cr = X, Tr = Cr, Sq = Tr, X = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// rotate face buttons clockwise
					// Sq = X, X = Cr, Cr = Tr, Tr = Sq
					// Select = Select/Share
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}
		}
		else if (OutputMode == Output_Switch)
		{
			// ButtonMap: Switch, arcade stick
			if (ControllerType == Controller_Arcade)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// button 1 = A
					// button 2 = B
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) // down
				{
					// button 1 = B
					// button 2 = A
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 3) // left
				{
					// button 1 = A
					// button 2 = Y
					// button 3 = B
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 6;
					buttonMap[6] = 4;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// button 1 = Y
					// button 2 = A
					// button 3 = B
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 5;
					buttonMap[6] = 4;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[14] = 14;
					buttonMap[15] = 15;
					buttonMap[16] = 16;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) { }
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}
			// ButtonMap: Switch, NES controller
			else if (ControllerType == Controller_NES)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) // down
				{
					// A = B
					// B = A
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 3) // left
				{
					// A = B
					// B = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// B = B
					// A = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// (passthrough)
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) // down+right
				{
					// A = B
					// B = A
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 4;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 7) // down+left
				{
					// A = B
					// B = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// B = B
					// A = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 6;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}
			// ButtonMap: Switch, SNES controller
			else if (ControllerType == Controller_SNES)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) // left
				{
					// rotate face buttons counter-clockwise
					// A = B, X = A, Y = X, B = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// rotate face buttons clockwise
					// Y = B, B = A, A = X, X = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// (passthrough)
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) // down+left
				{
					// rotate face buttons counter-clockwise
					// A = B, X = A, Y = X, B = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// rotate face buttons clockwise
					// Y = B, B = A, A = X, X = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}	
			// ButtonMap: Switch, Genesis Controller
			else if (ControllerType == Controller_Genesis)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// A = Y
					// B = B
					// C = A
					// X = L
					// Y = X
					// Z = R
					// Mode = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 8;
					buttonMap[8] = 7;
					buttonMap[9] = 9;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) { }
				else if (profile == 4) { }
				else if (profile == 5) // up+left
				{
					// A = Y
					// B = B
					// C = A
					// X = L
					// Y = X
					// Z = R
					// Mode = -
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 5;
					buttonMap[7] = 8;
					buttonMap[8] = 7;
					buttonMap[9] = 9;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) { }
				else if (profile == 8) { }
			}	
			// ButtonMap: Switch, PSX controller
			else if (ControllerType == Controller_PSX)
			{
				if (Player == 2)
					OutputDirectionalType = Directional_Left_Analog;
				else if (Player == 3)
					OutputDirectionalType = Directional_Right_Analog;
				else
					OutputDirectionalType = Directional_D_Pad;
			
				if (profile == 1) // up, default
				{
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 2) { }
				else if (profile == 3) // left
				{
					// rotate face buttons counter-clockwise
					// Cr = B, Tr = A, Sq = X, X = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 4) // right
				{
					// rotate face buttons clockwise
					// Sq = B, X = A, Cr = X, Tr = Y
					// Select = Home
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 14;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 5) // up+left
				{
					// (passthrough)
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 4;
					buttonMap[5] = 5;
					buttonMap[6] = 6;
					buttonMap[7] = 7;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 6) { }
				else if (profile == 7) // down+left
				{
					// rotate face buttons counter-clockwise
					// Cr = B, Tr = A, Sq = X, X = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 6;
					buttonMap[5] = 4;
					buttonMap[6] = 7;
					buttonMap[7] = 5;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
				else if (profile == 8) // up+right
				{
					// rotate face buttons clockwise
					// Sq = B, X = A, Cr = X, Tr = Y
					buttonMap[0] = 0;
					buttonMap[1] = 1;
					buttonMap[2] = 2;
					buttonMap[3] = 3;
					buttonMap[4] = 5;
					buttonMap[5] = 7;
					buttonMap[6] = 4;
					buttonMap[7] = 6;
					buttonMap[8] = 8;
					buttonMap[9] = 9;
					buttonMap[10] = 10;
					buttonMap[11] = 11;
					buttonMap[12] = 12;
					buttonMap[13] = 13;
					buttonMap[17] = 255;
					buttonMap[18] = 255;
					buttonMap[19] = 255;
				}
			}
		}
	
		// disable buttons which don't exist on current controller
		{	
			// NES controller
			if (ControllerType == Controller_NES)
			{
				if (OutputMode == Output_PC)
				{
					buttonMap[6] = KEY_NONE;
					buttonMap[7] = KEY_NONE;
					buttonMap[8] = KEY_NONE;
					buttonMap[9] = KEY_NONE;
					buttonMap[10] = KEY_NONE;
					buttonMap[11] = KEY_NONE;
					buttonMap[14] = KEY_NONE;
					buttonMap[15] = KEY_NONE;
					buttonMap[16] = KEY_NONE;
				}
				else
				{				
					buttonMap[6] = 255;
					buttonMap[7] = 255;
					buttonMap[8] = 255;
					buttonMap[9] = 255;
					buttonMap[10] = 255;
					buttonMap[11] = 255;
					buttonMap[14] = 255;
					buttonMap[15] = 255;
					buttonMap[16] = 255;
				}
			}
			// SNES/Genesis controller
			else if ((ControllerType == Controller_SNES) || (ControllerType == Controller_Genesis))
			{
				if (OutputMode == Output_PC)
				{
					buttonMap[10] = KEY_NONE;
					buttonMap[11] = KEY_NONE;
					buttonMap[14] = KEY_NONE;
					buttonMap[15] = KEY_NONE;
					buttonMap[16] = KEY_NONE;
				}
				else
				{
					buttonMap[10] = 255;
					buttonMap[11] = 255;
					buttonMap[14] = 255;
					buttonMap[15] = 255;
					buttonMap[16] = 255;
				}
			}
			// PSX controller
			else if (ControllerType == Controller_PSX)
			{
				if (OutputMode == Output_PC)
				{
					buttonMap[14] = KEY_NONE;
					buttonMap[15] = KEY_NONE;
					buttonMap[16] = KEY_NONE;
				}
				else
				{
					buttonMap[14] = 255;
					buttonMap[15] = 255;
					buttonMap[16] = 255;
				}
			}
		}
	}
	
	// convert buttonMap to ButtonMap
	{
		if (OutputMode == Output_PC)
		{
			for (uint8_t button = 0; button < 8; button++)
				ButtonMap[button][0] = buttonMap[button];
				
			for (uint8_t button = 0; button < 8; button++)
				ButtonMap[button][1] = buttonMap[button + 8];
				
			for (uint8_t button = 0; button < 4; button++)
				ButtonMap[button][2] = buttonMap[button + 16];
		}
		else
		{
			// default ButtonMap to empty
			for (uint8_t button = 0; button < 18; button++)
				for (uint8_t index = 0; index < 3; index++)
					ButtonMap[button][index] = 0b11111111;
			
			for (uint8_t button = 0; button < 20; button++)
				if (buttonMap[button] < 18)
				{
					uint8_t bit = button;
					uint8_t index = 0;
				
					// adjust bit and index to select position in ButtonMap
					while (bit > 7)
					{
						bit -= 8;
						index++;
					}
				
					ButtonMap[buttonMap[button]][index] = ButtonMap[buttonMap[button]][index] & ~(0b0000001 << bit);
				}
		}
	}
	
}